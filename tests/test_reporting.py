# -*- coding: utf-8 -*-

import pytest

from pytest_reporting.behaviors import Sum, Count, List, Median


@pytest.fixture
def reporting(reporting):
    config = {
        "columns": [Sum("first"), Count("second"), List("third"), Median("third")],
        "display_total": True,
    }

    reporting.configure(report="A", config=config)

    return reporting


def test_report_a_1(reporting):
    reporting.add(
        report="A", variables={"state": "std"}, first=2, second="thing_1", third=5
    )


def test_report_a_2(reporting):
    reporting.add(report="A", variables={"state": "std"}, first=1, third=2)


def test_report_a_3(reporting):
    reporting.add(report="A", variables={"state": "std", "state2": "other"}, first=1)


def test_report_b_1(reporting):
    reporting.add(report="B", variables={"state": "std"}, first=4)


def test_report_b_2(reporting):
    reporting.add(report="B", variables={"state": "std"}, first=5)
